
var lpApp = angular.module('lpApp',[]);

lpApp.controller('lpPriceCtrl', ['$scope','$http', function($scope, $http){

    $scope.sortBy='name';
    $scope.sortRev=false;

    $http.get('price.json').then(function (response) {
        $scope.prices=response.data;
        $scope.calc();
        console.log(response)
    });

    $scope.calc=function () {
        $scope.prices.forEach( function(price, index) {
            $scope.prices[index].sum=$scope.prices[index].price*(1-$scope.prices[index].discount);
        });
        console.log($scope.prices);
        
    }

    $scope.sortSet = function (propertyName) {
        if ($scope.sortBy == propertyName) {
            $scope.sortRev=!$scope.sortRev;
        }
        $scope.sortBy=propertyName;
    }
}]);



(function ($) {
	$(document).ready(function() {

		function lpHeader () {
			if ($(window).scrollTop() == 0) {
				$('header').addClass('top');
			} else {
				$('header.top').removeClass('top');
			}
		}
		lpHeader();
		$(window).on('scroll',lpHeader);
				
		

		var lpMenuItems=$('header ul.nav li a');
		lpMenuItems.each(function() {
			var link = $(this),
				linkHref = link.attr('href'),
				linkTrgt = $(linkHref);
			if (linkTrgt.length > 0) {
				link.on('click', function(event) {
					event.preventDefault();
					var offset = linkTrgt.offset().top;
					var offset = linkTrgt.offset().top;
					$('html,body').animate({scrollTop:offset}, 1000);
				
				});
			}
		});
		


 		var lpNavItems = [];
        	function lpSetNavItems() {
           		lpNavItems = [];
           		$('section').each(function () {
               		lpNavItems.push({
                   		top: $(this).offset().top, 
                   		name: $(this).attr('id') 
               		});
           		});
        	}
        lpSetNavItems(); 
        $(window).on('resize', lpSetNavItems); 



        function lpSetNavActive() {
            var curItem = '';
            lpNavItems.forEach(function (item) {
                if ($(window).scrollTop() > item.top-200) {
                    curItem = item.name;
                }
            });
            if ($('ul.nav li.active a').attr('href') != '#' + curItem || $('ul.nav li.active').length == 0) {
                $('ul.nav li.active').removeClass('active');
                $('ul.nav li a[href="#' + curItem + '"]').parent().addClass('active');
            }
            
        };
        lpSetNavActive();
        $(window).on('scroll', lpSetNavActive);




        $('.lp-slider1').owlCarousel({
        	items:2,
        	nav: true,
        	navText:['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        	loop: true,
        	dotsEach:true,
        	animateOut: 'fadeOut'
        });



        $('.lp-slider2').on('initialized.owl.carousel', function() {
        	console.log('initialized.owl.carousel');
        });



        $('.lp-slider2').owlCarousel({
        	items:3,
        	nav: true,
        	navText:['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        	/*loop: true,*/
        	dotsEach:true,
        	responsive : {
    			768 : {
        			items:3
        		},
   				480 : {
    			    items:2
       			},
    			0 : {
    			    items:1
    			}
			}
		});
       /* $('.lp-slider3').owlCarousel({
        	items:5,
        	nav: true,
        	navText:['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        	loop: true,
        	dotsEach:true,
        	responsive : {
    			768 : {
        			items:5
        		},
   				480 : {
    			    items:4
       			},
    			0 : {
    			    items:3
    			}
			}
		});*/
    	


        $('.lp-slider2').on('changed.owl.carousel', function() {
        	console.log('changed.owl.carousel');
        });



        $('.lp-slider2').on('resized.owl.carousel', function() {
        	console.log('resized.owl.carousel');
        });



        var inputNumberOfSlide=document.getElementById('number');
        console.log(inputNumberOfSlide)
        $('#btnSlider').on('click', function() {
        	var value = Number(inputNumberOfSlide.value)
        	console.log(value);
        	if (value>0 && value<11) {
        		$('.lp-slider2').trigger('to.owl.carousel', value-1);
        	}else if(isNaN(value)){
        		console.log('Not a NUMBER!!')
        	}else if(value>10||value<1){
        		console.log('enter letter between 0 and 11')
        	}
        });



        $('.lp-slider2').on('dragged.owl.carousel',callback1);
        function callback1(event) {
    		var page      = event.page.index;
    		console.log(page)
    		$('.lp-slider3').trigger('to.owl.carousel', page);
		}
        $('.lp-slider3').on('changed.owl.carousel',callback2);
        function callback2(event) {
    		var page      = event.page.index;
    		console.log(page)
    		$('.lp-slider2').trigger('to.owl.carousel', page);
		}
		
		$('.lp-tabs').each(function() {
			var tabs=$(this),
			tabsTitlesNames=[];

			tabs.find('div[data-tab-title]').each(function() {	
				tabsTitlesNames.push($(this).attr('data-tab-title'));
			}).addClass('lp-tab');

			tabs.wrapInner('<div class="lp-tabs-content"></div>');

			tabs.prepend('<div class="lp-tabs-titles"><ul></ul></div>');

			var tabsTitles = tabs.find('.lp-tabs-titles'),
				tabsContent = tabs.find('.lp-tabs-content'),
				tabsContentTabs = tabs.find('div[data-tab-title]');

			tabsTitlesNames.forEach(function(value) {
				tabsTitles.find('ul').append('<li>'+value+'</li>')
			});

			var tabsTitlesItems=tabsTitles.find('ul li');
			tabsTitlesItems.eq(0).addClass('active');
			tabsContentTabs.eq(0).addClass('active').show();
			tabsContent.height(tabsContent.find('.active').outerHeight());
			tabsTitlesItems.hover(function() {
				if (!tabs.hasClass('changing')) {
					tabs.addClass('changing');

					var curTab = tabsContent.find('.active'),
						nextTab = tabsContentTabs.eq($(this).index());

					tabsTitlesItems.removeClass('active');
					$(this).addClass('active');

					var curHeight=curTab.outerHeight();
					nextTab.show();
				
					var nextHeight = nextTab.outerHeight();
					nextTab.hide();

					if (curHeight<nextHeight) {
						tabsContent.animate({height:nextHeight},500);
					}


					curTab.fadeOut(500, function() {
						if (curHeight>nextHeight) {
							tabsContent.animate({height:nextHeight},500);
						}

					nextTab.fadeIn(500, function() {
						curTab.removeClass('active');
						nextTab.addClass('active');
						tabs.removeClass('changing');
					});

					var footerInf = $('div.lp-tabs-titles>ul li.active').text(),
						tabFooter = $('.lp-tabs-footer');

					if ($('div.lp-tabs-titles>ul li').hasClass('active')) {
						tabFooter.text('Active tab:'+' '+footerInf);
					}
				});
			}
		});
		$(window).on('resize', function () {
        	tabsContent.height(tabsContent.find('.active').outerHeight());
		});
	});        
	

	$('.lp-mfp-inline').magnificPopup({
 		 type: 'inline'
 	});
	$('.lp-gallery').each(function() {
		 $(this).magnificPopup({
 			type: 'image',
 			delegate:'a',
  			gallery:{
   			enabled:true
  			}
		});
	});

	$('.ajax-popup-link').magnificPopup({
		type: 'ajax',

	});

	$('.video').magnificPopup({
  		type: 'iframe'
  	});


	var ex3ButtonText = {};
	function pushingButtonsNames() {
        ex3ButtonText.name = $(this).text()
        console.log(ex3ButtonText.name);
    }

	$('.ajax-popup-link.ex3Button').magnificPopup({
		type: 'ajax',	
		callbacks: {
  			ajaxContentAdded: function() {
               	$('div.ex3Html>h2').text(ex3ButtonText.name)
               	console.log(this.content);
        	}
    	}
	});

	$('.ex3Button').on('click', pushingButtonsNames);
	

	$('.gallery').each(function() {
    	$(this).magnificPopup({
        	delegate:'a',
        	type: 'image',
        	gallery: {
          enabled:true
        	}
    	});
	});
	$('.ex4Button').on('click', function() {

	});

	$(document).ready(function () {
            $('#lp-fb1').wiFeedBack({
                fbScript: 'blocks/wi-feedback.php',
                fbLink: false,
                fbColor: '#618ad2'
            });
              $('#lp-fb2').wiFeedBack({
                fbScript: 'blocks/wi-feedback.php',
                fbLink: '.lp-fb2-link',
                fbColor: '#618ad2'
            });
              $('#lp-fb3').wiFeedBack({
                fbScript: 'blocks/wi-feedback.php',
                fbLink: '.lp-fb2-link',
                fbColor: '#618ad2'
            });
              $('#lp-fb4').wiFeedBack({
                fbScript: 'blocks/wi-feedback.php',
                fbLink: '.lp-fb2-link',
                fbColor: '#618ad2'
            });
    });


	
	var serviceNames = {};
	function capturingServiceNames() {
        serviceNames.name = $(this).attr('data-wi-fb-info');
        console.log(serviceNames.name);
        $('div [id="lp-fb2"] h2').text(serviceNames.name)
    }
    $('a.lp-fb2-link').on('click', capturingServiceNames);



	});
})(jQuery);